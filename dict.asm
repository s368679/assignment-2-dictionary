%include "lib.inc"

section .rodata
%define POINT_SIZE 8

section .text

global find_word

;Принимает два аргумента:
;Указатель на нуль-терминированную строку.
;Указатель на начало словаря.
;Проходит по всему словарю в поисках подходящего ключа. Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.
find_word:
    push rbx
    push r12
    mov rbx, rdi;       сохраним аргументы в calee-saved регистры,
    mov r12, rsi
    .iteration:
        test r12, r12;          if node == 0 go to break
        je .break
        mov rdi, rbx
        mov rsi, r12
        add rsi, POINT_SIZE;             переходим к адресу ключа
        call string_equals   
        test rax, rax
        jne .break
        mov r12, [r12];         переходим к следующему елементу
        jmp .iteration
    .break:
        mov rax, r12
        pop r12
        pop rbx;                clear
        ret