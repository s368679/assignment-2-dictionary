%include "words.inc"
%include "dict.inc"
%include "lib.inc"

section .rodata
%define POINT_SIZE 8
%define BUFF_SIZE 256;      размер буфера
key_too_long_err: db "Key is too long", 0
dict_end_err: db "Key not found", 0

section .bss
input_buff: resb BUFF_SIZE

section .text
global _start

_start:
    mov rdi, input_buff;        buff pointer -> rdi
    mov rsi, BUFF_SIZE;         buff size -> rsi
    call read_word;             прочитали слово
    test rax, rax;             
    je .too_long;
    push rdx;                   сохранили длину строки
    mov rdi, rax;               
    mov rsi, HEAD_NODE;         
    call find_word
    pop rdx;                   
    test rax, rax;            
    je .not_found
    add rax, POINT_SIZE;        перешли к ключу
    add rax, rdx;               pointer + длина строки
    inc rax;                    пропустили 0 terminator
    mov rdi, rax
    call print_string
    call print_newline
    .end:
        xor rdi, rdi
        call exit

    .too_long:
        mov rdi, key_too_long_err
        jmp .handle_error

    .not_found:
        mov rdi, dict_end_err
    
    .handle_error:
        call print_error
        call print_newline
        jmp .end