global exit
global string_length
global print_string
global print_error
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

%define s_exit 60
%define s_write 1
%define s_stdout 1
%define devisor_of_num 10 ; Делитель числа для перевода из 16сс в 10сс
%define s_read 0
%define s_stdin 0
%define s_stderr 2;

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, s_exit
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    je .string_length_end
    inc rax
    jmp .loop
.string_length_end:    
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rdi, s_stdout
    mov rax, s_write
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stderr
print_error:
    push rdi;               save rdi before calling function
    call string_length;     str length -> rax
    mov rdx, rax;           str length -> rdx (syscall arg)
    mov rax, s_write;     write system code -> rax
    pop rsi;                string start from stack -> rsi (syscall arg)
    mov rdi, s_stderr;        error output -> rdi (syscall arg)
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov dil, 0xA

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint
    
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r8, devisor_of_num
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    sub rsp, 24 ; Выделяем место в стеке
    mov byte[rdi], 0
.loop:
    xor rdx, rdx
    div r8
    or dl, 48
    dec rdi
    mov [rdi], dl
    test rax, rax
    jnz .loop
    call print_string
    add rsp, 24
    ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte[rdi]
    mov cl, byte[rsi]
    inc rdi
    inc rsi
    cmp al, cl
    je .finishing
    xor rax, rax
    ret
  .finishing:
    test al, al
    jnz string_equals
    mov rax, 1
    ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rdx, 1
    mov rsi, rsp
    mov rdi, s_stdin
    mov rax, s_read
    syscall
    pop rax
    ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12                        
    push r13                            
    push r14
    push rdi                     
    mov r12, rdi
    mov r13, rsi
    mov r14, rcx                
    .delete_spaces:                   
        call read_char
        cmp rax, 0x20                
        je .delete_spaces
        cmp rax, 0x9
        je .delete_spaces
        cmp rax, 0xA
        je .delete_spaces
        xor r14, r14                   
    .read_word_loop:
        inc r14                                            
        cmp r14, r13            
        jg .read_word_err  
        mov byte[r12], al 
        cmp rax, 0
        je .read_word_end
        cmp rax, 0x20
        je .read_word_end
        cmp rax, 0x9
        je .read_word_end
        cmp rax, 0xA
        je .read_word_end  
        inc r12                  
        call read_char
        jmp .read_word_loop
    .read_word_err:     
        pop rdi                 
        mov rax, 0                     
        jmp .end
    .read_word_end:                  
        mov rdx, r14
        dec rdx 
        pop rax
    .end:
        pop r14
        pop r13
        pop r12
        ret



 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    mov r8, devisor_of_num
    xor rcx, rcx
.parse_num:
    mov sil, byte[rdi+rcx]
    test sil, sil
    sub sil, '0'                     
    cmp sil, 0                       
    jl .parse_uint_end                      
    cmp sil, 9                       
    jg .parse_uint_end
    inc rcx
    mul r8
    add rax, rsi
    jmp .parse_num
.parse_uint_end:
    mov rdx, rcx
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov cl, byte[rdi]
    cmp cl, '-'
    je .if_have
    cmp cl, '+'
    je .if_have
    jmp .parsing
    .if_have:
        inc rdi        
    .parsing:
        push rcx                 
        call parse_uint         
        pop rcx                     
        cmp cl, '-'
        je .parse_int_end
        ret
    .parse_int_end:
        neg rax  
        inc rdx 
        ret 


 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jnb .overflow
    mov rcx, rax
.copy_next:
    mov r8b, [rdi + rcx]
    mov [rsi + rcx], r8b
    sub rcx, 1
    jnc .copy_next
    ret
.overflow:
    xor rax, rax        
    ret

