from subprocess import *

inputs: list = ["Lab_1", "Lab_2", "Lab_3", "Lab 4", "Lab_55555555555555555555555555555555555555555555555551123133333333333333333333333333313333333333333333333333333333333333333133333333333333333333333335555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555555"]
outputs: list = ["Easy", "Not enough hard", "I don't know, I don't do it", "", ""]
errors: list = ["", "", "", "Key not found", "Key is too long"]

def build_program() -> None:
    process: CompletedProcess = run(["make", "main"], stdout=DEVNULL, stderr=DEVNULL)
    try:
        process.check_returncode()
        print("Compiled successfully.")
    except CalledProcessError:
        print(f"Compilation finished with code {process.returncode}.")
        exit()

def test() -> None:
    failed_count: int = 0
    tests_count: int = len(inputs)
    
    build_program()

    for i in range(len(inputs)):
        p = Popen(['./main'], stdin=PIPE, stdout=PIPE, stderr = PIPE, text=True)
        output, error = map(lambda out: out.strip(), p.communicate(input = inputs[i]))
        if output == outputs[i] and error == errors[i]:
            print(f"Test {i + 1}... finished with code 0.")
        else:
            failed_count += 1
            if (outputs[i]):
                print(f"Test {i + 1}... failed. \nExpected output '{outputs[i]}' when given output is '{output}'.")
            else:
                print(f"Test {i + 1}... failed. \nExpected error '{errors[i]}' when given output is '{error}'.")
    print("-" * 20)
    if (failed_count):
        print(f"Failed {failed_count} tests out of {tests_count}.")
    else:
        print(f"All test passed.")


test()
